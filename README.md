
# Project Name

This project was created by [SoftGen.ai](http://www.SoftGen.ai).

## Getting Started

To get the project up and running on your local machine for development and testing purposes, follow the instructions below.

### Backend

To run the backend, execute the following commands:
bash
npm install
npm start


### Frontend

To run the frontend in development mode, use the following commands:
bash
npm install
npm run dev

To build the frontend for production, run:
bash
npm run build
npm start
